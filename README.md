# Unit test generator
[![pipeline status](https://gitlab.com/ValeryKameko/unit-test-generator/badges/master/pipeline.svg)](https://gitlab.com/ValeryKameko/unit-test-generator/commits/master)
[![coverage report](https://gitlab.com/ValeryKameko/unit-test-generator/badges/master/coverage.svg)](https://gitlab.com/ValeryKameko/unit-test-generator/commits/master)

Unit test generator for NUnit framework 