using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UnitTestGenerator.Generator
{
    public class DefaultValueGenerator
    {
        private static readonly ExpressionSyntax ZeroExpression =
            LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(0));

        private static readonly ExpressionSyntax NullExpression =
            LiteralExpression(SyntaxKind.NullLiteralExpression);

        private static readonly ExpressionSyntax FalseExpression =
            LiteralExpression(SyntaxKind.FalseLiteralExpression);

        private static readonly LiteralExpressionSyntax EmptyStringExpression =
            LiteralExpression(SyntaxKind.StringLiteralExpression, Literal(""));

        private static readonly LiteralExpressionSyntax DefaultCharExpression =
            LiteralExpression(SyntaxKind.CharacterLiteralExpression, Literal('\0'));

        private static readonly Dictionary<SyntaxKind, ExpressionSyntax> DefaultValueByTypeName =
            new Dictionary<SyntaxKind, ExpressionSyntax>
            {
                {SyntaxKind.ObjectKeyword, NullExpression},
                {SyntaxKind.BoolKeyword, FalseExpression},
                {SyntaxKind.StringKeyword, EmptyStringExpression},
                {SyntaxKind.CharKeyword, DefaultCharExpression},
                {SyntaxKind.DecimalKeyword, ZeroExpression},
                {SyntaxKind.FloatKeyword, ZeroExpression},
                {SyntaxKind.DoubleKeyword, ZeroExpression},
                {SyntaxKind.ByteKeyword, ZeroExpression},
                {SyntaxKind.SByteKeyword, ZeroExpression},
                {SyntaxKind.ShortKeyword, ZeroExpression},
                {SyntaxKind.UShortKeyword, ZeroExpression},
                {SyntaxKind.IntKeyword, ZeroExpression},
                {SyntaxKind.UIntKeyword, ZeroExpression},
                {SyntaxKind.LongKeyword, ZeroExpression},
                {SyntaxKind.ULongKeyword, ZeroExpression},
            };

        public ExpressionSyntax Generate(TypeSyntax type)
        {
            switch (type)
            {
                case ArrayTypeSyntax arrayType:
                    return InitializerExpression(SyntaxKind.ArrayInitializerExpression);
                case PredefinedTypeSyntax predefinedType:
                    return DefaultValueByTypeName[predefinedType.Keyword.Kind()];
                case NullableTypeSyntax nullableType:
                    return Generate(nullableType.ElementType);
                case NameSyntax namedType:
                    return NullExpression;
                case TupleTypeSyntax tupleType:
                    return GenerateTupleDefaultValue(tupleType);
                default:
                    return null;
            }
        }

        private ExpressionSyntax GenerateTupleDefaultValue(TupleTypeSyntax tupleType)
        {
            IEnumerable<ArgumentSyntax> tupleArguments =
                tupleType.Elements
                    .Select(element => Generate(element.Type))
                    .Select(Argument);
            return TupleExpression(SeparatedList(tupleArguments));
        }
    }
}