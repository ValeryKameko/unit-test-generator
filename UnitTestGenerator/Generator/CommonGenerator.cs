using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;


namespace UnitTestGenerator.Generator
{
    internal static class CommonGenerator
    {
        public static StatementSyntax GenerateVariableDeclaration(TypeSyntax type, string identifier,
            ExpressionSyntax initialValue) =>
            LocalDeclarationStatement(
                VariableDeclaration(type)
                    .AddVariables(
                        VariableDeclarator(identifier)
                            .WithInitializer(
                                EqualsValueClause(initialValue)
                            )));
    }
}