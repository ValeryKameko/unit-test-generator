using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UnitTestGenerator.Entity;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using ArgumentSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.ArgumentSyntax;
using AttributeListSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.AttributeListSyntax;
using AttributeSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.AttributeSyntax;
using ExpressionSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.ExpressionSyntax;
using FieldDeclarationSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.FieldDeclarationSyntax;
using GenericNameSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.GenericNameSyntax;
using NameSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.NameSyntax;
using StatementSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.StatementSyntax;
using TypeSyntax = Microsoft.CodeAnalysis.CSharp.Syntax.TypeSyntax;

namespace UnitTestGenerator.Generator
{
    internal class ClassTestGenerator
    {
        private const string TestClassSuffix = "Tests";
        private const string SetUpIdentifier = "SetUp";

        private static readonly AttributeSyntax TestFixtureAttribute =
            Attribute(IdentifierName("TestFixture"));

        private static readonly AttributeListSyntax TestFixtureAttributesList =
            AttributeList(SingletonSeparatedList(TestFixtureAttribute));

        private readonly DefaultValueGenerator _defaultValueGenerator =
            new DefaultValueGenerator();

        public ClassDeclarationSyntax Generate(TestingClassInfo testingClassInfo)
        {
            return ClassDeclaration(testingClassInfo.Identifier + TestClassSuffix)
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
                .AddAttributeLists(TestFixtureAttributesList)
                .AddMembers(GenerateClassUnderTestField(testingClassInfo))
                .AddMembers(GenerateMockFields(testingClassInfo).ToArray())
                .AddMembers(GenerateSetUpMethod(testingClassInfo));
        }

        private IEnumerable<MemberDeclarationSyntax> GenerateMockFields(TestingClassInfo testingClassInfo)
        {
            TestingConstructorInfo constructor = testingClassInfo.MostParametersConstructor;
            if (constructor == null)
                yield break;
            foreach (TestingParameterInfo parameter in constructor.DependencyParameters)
                yield return GenerateMockField(parameter);
        }

        private MemberDeclarationSyntax GenerateMockField(TestingParameterInfo parameter)
        {
            return FieldDeclaration(
                    VariableDeclaration(GenerateMockType(parameter.Type))
                        .AddVariables(VariableDeclarator(
                            ConstructMockFieldIdentifier(parameter.Identifier))))
                .AddModifiers(Token(SyntaxKind.PrivateKeyword));
        }

        private static GenericNameSyntax GenerateMockType(TypeSyntax type) =>
            GenericName("Mock").AddTypeArgumentListArguments(type.WithoutTrivia());

        private string ConstructMockFieldIdentifier(string parameterIdentifier)
        {
            string lowerCamelCaseIdentifier = ConstructLowerCamelCaseIdentifier(parameterIdentifier);
            return $"_{lowerCamelCaseIdentifier}";
        }

        private static string ConstructLowerCamelCaseIdentifier(string identifier) =>
            char.ToLower(identifier.First()) + identifier.Substring(1);


        private MemberDeclarationSyntax GenerateSetUpMethod(TestingClassInfo testingClass)
        {
            TestingConstructorInfo constructor = testingClass.MostParametersConstructor;
            MethodDeclarationSyntax setUpMethod = MethodDeclaration(
                    PredefinedType(Token(SyntaxKind.VoidKeyword)),
                    SetUpIdentifier)
                .AddAttributeLists(AttributeList(SingletonSeparatedList(
                    Attribute(IdentifierName(SetUpIdentifier))
                )))
                .AddModifiers(Token(SyntaxKind.PrivateKeyword))
                .AddBodyStatements();

            if (constructor != null)
                setUpMethod = setUpMethod.AddBodyStatements(
                    GenerateConstructorParameterDeclarations(constructor).ToArray())
                .AddBodyStatements(GenerateConstructorInvocation(testingClass));
            return setUpMethod;
        }

        private StatementSyntax GenerateConstructorInvocation(TestingClassInfo testingClass)
        {
            string testingClassIdentifier = ConstructClassUnderTestIdentifier(testingClass.Identifier);
            ExpressionSyntax assignment = AssignmentExpression(
                SyntaxKind.SimpleAssignmentExpression,
                IdentifierName(testingClassIdentifier),
                ObjectCreationExpression(IdentifierName(testingClass.FullIdentifier))
                    .AddArgumentListArguments(GenerateConstructorArguments(testingClass.MostParametersConstructor)
                        .ToArray())
            );
            return ExpressionStatement(assignment);
        }

        private IEnumerable<ArgumentSyntax> GenerateConstructorArguments(TestingConstructorInfo constructor)
        {
            foreach (TestingParameterInfo parameter in constructor.Parameters)
            {
                if (parameter.Type is NameSyntax)
                    yield return Argument(MemberAccessExpression(
                        SyntaxKind.SimpleMemberAccessExpression,
                        IdentifierName(ConstructMockFieldIdentifier(parameter.Identifier)),
                        IdentifierName("Object")));
                else
                    yield return Argument(IdentifierName(
                        ConstructLowerCamelCaseIdentifier(parameter.Identifier)));
            }
        }

        private IEnumerable<StatementSyntax> GenerateConstructorParameterDeclarations(
            TestingConstructorInfo constructor)
        {
            if (constructor == null) yield break;

            foreach (TestingParameterInfo parameter in constructor.DependencyParameters)
                yield return GenerateDependencyParameterDeclaration(parameter);

            IEnumerable<TestingParameterInfo> otherParameters =
                constructor.Parameters.Except(constructor.DependencyParameters);
            foreach (TestingParameterInfo parameter in otherParameters)
                yield return GenerateDefaultTypeParameterDeclaration(parameter);
        }

        private StatementSyntax GenerateDefaultTypeParameterDeclaration(TestingParameterInfo parameter)
        {
            string identifier = ConstructLowerCamelCaseIdentifier(parameter.Identifier);
            ExpressionSyntax initialValue = _defaultValueGenerator.Generate(parameter.Type);

            return CommonGenerator.GenerateVariableDeclaration(parameter.Type, identifier, initialValue);
        }

        private StatementSyntax GenerateDependencyParameterDeclaration(TestingParameterInfo parameter)
        {
            TypeSyntax mockType = GenerateMockType(parameter.Type);
            string identifier = ConstructMockFieldIdentifier(parameter.Identifier);

            ExpressionSyntax assignment = AssignmentExpression(
                SyntaxKind.SimpleAssignmentExpression,
                IdentifierName(identifier),
                ObjectCreationExpression(mockType)
                    .AddArgumentListArguments());

            return ExpressionStatement(assignment);
        }

        private FieldDeclarationSyntax GenerateClassUnderTestField(TestingClassInfo testingClassInfo)
        {
            return FieldDeclaration(
                    VariableDeclaration(IdentifierName(testingClassInfo.FullIdentifier))
                        .AddVariables(
                            VariableDeclarator(ConstructClassUnderTestIdentifier(testingClassInfo.Identifier))))
                .AddModifiers(Token(SyntaxKind.PrivateKeyword));
        }

        private string ConstructClassUnderTestIdentifier(string testingClassIdentifier)
        {
            string lowerCamelCaseIdentifier = ConstructLowerCamelCaseIdentifier(testingClassIdentifier);
            return $"_{lowerCamelCaseIdentifier}UnderTest";
        }
    }
}