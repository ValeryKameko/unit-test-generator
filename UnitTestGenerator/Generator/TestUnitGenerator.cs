using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UnitTestGenerator.Entity;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UnitTestGenerator.Generator
{
    internal class TestUnitGenerator
    {
        private static readonly List<string> MustHaveUsingNamespaces = new List<string>
        {
            "System",
            "System.Collections.Generic",
            "System.Linq",
            "System.Text",
            "NUnit.Framework",
            "Moq",
        };

        private readonly ClassTestGenerator _classGenerator =
            new ClassTestGenerator();

        public IEnumerable<CompilationUnitSyntax> Generate(TestingUnitInfo testingUnit)
        {
            foreach (TestingClassInfo testingClass in testingUnit.Classes)
                yield return CompilationUnit()
                    .AddUsings(GenerateUsings(testingUnit, testingClass).ToArray())
                    .AddMembers(
                        NamespaceDeclaration(ParseName(ConstructTestNamespace(testingClass)))
                            .AddMembers(_classGenerator.Generate(testingClass)));
        }

        private static string ConstructTestNamespace(TestingClassInfo testingClass) =>
            $"{testingClass.Namespace}.Tests";

        private IEnumerable<UsingDirectiveSyntax> GenerateUsings(
            TestingUnitInfo testingUnit,
            TestingClassInfo testingClass)
        {
            ICollection<string> usingNamespaces = new HashSet<string>();
            foreach (string unitUsingNamespace in testingUnit.UsingNamespaces)
                usingNamespaces.Add(unitUsingNamespace);
            foreach (string mustHaveUsingNamespace in MustHaveUsingNamespaces)
                usingNamespaces.Add(mustHaveUsingNamespace);

            foreach (string usingNamespace in usingNamespaces)
                yield return UsingDirective(ParseName(usingNamespace));
            yield return UsingDirective(ParseName(testingClass.Namespace));
        }
    }
}