using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using UnitTestGenerator.Entity;
using UnitTestGenerator.Generator;

namespace UnitTestGenerator
{
    public class TestUnitGeneratorBlock : IPropagatorBlock<CompilationItem, CompilationItem>
    {
        private readonly ISourceBlock<CompilationItem> _sourceBlock;
        private readonly ITargetBlock<CompilationItem> _targetBlock;

        private readonly TestUnitGenerator _generator = new TestUnitGenerator();

        public TestUnitGeneratorBlock(ExecutionDataflowBlockOptions config)
        {
            var sourceBlock = new BufferBlock<CompilationItem>();
            var targetBlock = new ActionBlock<CompilationItem>(unitSource =>
            {
                CompilationUnitSyntax unit = SyntaxFactory.ParseCompilationUnit(unitSource.Source);
                TestingUnitInfo testingUnit = new TestingUnitInfo(unit);
                foreach (CompilationUnitSyntax testUnit in _generator.Generate(testingUnit))
                {
                    string testUnitSource = testUnit.NormalizeWhitespace().ToFullString();
                    sourceBlock.Post(new CompilationItem(ConstructTestFilePath(unitSource.Path, testUnit), testUnitSource));
                }
            }, config);

            _sourceBlock = sourceBlock;
            _targetBlock = targetBlock;
        }

        private static string ConstructTestFilePath(string path, CompilationUnitSyntax unit)
        {
            string className = unit.DescendantNodes().OfType<ClassDeclarationSyntax>()
                .Single().Identifier.ToString();
            return Path.Combine(Path.GetDirectoryName(path), $"{className}.{Path.GetExtension(path)}");
        }

        #region ITargetBlock<CompilationUnitSyntax> members

        DataflowMessageStatus ITargetBlock<CompilationItem>.OfferMessage(
            DataflowMessageHeader messageHeader,
            CompilationItem messageValue,
            ISourceBlock<CompilationItem> source, bool consumeToAccept) =>
            _targetBlock.OfferMessage(messageHeader, messageValue, source, consumeToAccept);

        #endregion

        #region IDataflowBlock members

        public void Complete() => _targetBlock.Complete();

        public void Fault(Exception exception) => _targetBlock.Fault(exception);

        public Task Completion => _sourceBlock.Completion;

        #endregion

        #region ISourceBlock<CompilationItem> members

        CompilationItem ISourceBlock<CompilationItem>.ConsumeMessage(
            DataflowMessageHeader messageHeader,
            ITargetBlock<CompilationItem> target,
            out bool messageConsumed) =>
            _sourceBlock.ConsumeMessage(messageHeader, target, out messageConsumed);

        IDisposable ISourceBlock<CompilationItem>.LinkTo(
            ITargetBlock<CompilationItem> target,
            DataflowLinkOptions linkOptions) =>
            _sourceBlock.LinkTo(target, linkOptions);

        void ISourceBlock<CompilationItem>.ReleaseReservation(
            DataflowMessageHeader messageHeader,
            ITargetBlock<CompilationItem> target) =>
            _sourceBlock.ReleaseReservation(messageHeader, target);

        bool ISourceBlock<CompilationItem>.ReserveMessage(
            DataflowMessageHeader messageHeader,
            ITargetBlock<CompilationItem> target) =>
            _sourceBlock.ReserveMessage(messageHeader, target);

        #endregion
    }
}