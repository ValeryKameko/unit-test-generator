using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UnitTestGenerator.Entity
{
    internal class TestingConstructorInfo
    {
        public TestingConstructorInfo(ConstructorDeclarationSyntax constructorDeclaration)
        {
            Parameters = constructorDeclaration.ParameterList.Parameters
                .Select(parameter => new TestingParameterInfo(parameter))
                .ToArray();
        }

        public IEnumerable<TestingParameterInfo> Parameters { get; }

        public IEnumerable<TestingParameterInfo> DependencyParameters =>
            Parameters.Where(parameter => parameter.Type is NameSyntax);
    }
}