using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UnitTestGenerator.Entity
{
    internal class TestingUnitInfo
    {
        public TestingUnitInfo(CompilationUnitSyntax unit)
        {
            UsingNamespaces = unit.Usings.Select(usingDirective => usingDirective.Name.ToFullString());
            Classes = unit.DescendantNodes().OfType<ClassDeclarationSyntax>()
                .Select(classDeclaration => new TestingClassInfo(classDeclaration));
        }

        public IEnumerable<string> UsingNamespaces { get; }
        public IEnumerable<TestingClassInfo> Classes { get; }
    }
}