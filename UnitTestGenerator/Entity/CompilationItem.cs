namespace UnitTestGenerator.Entity
{
    public class CompilationItem
    {
        public CompilationItem(string path, string source)
        {
            Path = path;
            Source = source;
        }

        public string Path { get; }
        public string Source { get; }
    }
}