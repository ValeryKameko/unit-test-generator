using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UnitTestGenerator.Entity
{
    internal class TestingParameterInfo
    {
        public TestingParameterInfo(ParameterSyntax parameter)
        {
            Identifier = parameter.Identifier.Text;
            ModifierKeywordKinds = parameter.Modifiers.Select(modifier => modifier.Kind());
            Type = parameter.Type;
        }
    
        public string Identifier { get; }
        public IEnumerable<SyntaxKind> ModifierKeywordKinds { get; }
        public TypeSyntax Type { get; }
    }
}