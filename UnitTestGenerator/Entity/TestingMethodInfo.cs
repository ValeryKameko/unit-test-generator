using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UnitTestGenerator.Entity
{
    internal class TestingMethodInfo
    {
        public TestingMethodInfo(MethodDeclarationSyntax method)
        {
            Identifier = method.Identifier.Text;
            Parameters = method.ParameterList.Parameters
                .Select(parameter => new TestingParameterInfo(parameter));
            ReturnType = method.ReturnType;
        }

        public string Identifier { get; }
        public IEnumerable<TestingParameterInfo> Parameters { get; }
        public TypeSyntax ReturnType { get; }
    }
}