using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UnitTestGenerator.Entity
{
    internal class TestingClassInfo
    {
        public TestingClassInfo(ClassDeclarationSyntax classDeclaration)
        {
            Identifier = classDeclaration.Identifier.Text;
            FullIdentifier = GenerateFullIdentifier(classDeclaration);
            Namespace = GenerateNamespaceName(classDeclaration);

            Constructors = classDeclaration.DescendantNodes().OfType<ConstructorDeclarationSyntax>()
                .Select(constructorDeclaration => new TestingConstructorInfo(constructorDeclaration));
            MostParametersConstructor = Constructors
                .OrderByDescending(constructor => constructor.Parameters.Count())
                .FirstOrDefault();
        }

        private string GenerateFullIdentifier(ClassDeclarationSyntax classDeclaration)
        {
            IEnumerable<ClassDeclarationSyntax> outerClasses =
                classDeclaration.AncestorsAndSelf().OfType<ClassDeclarationSyntax>().Reverse();
            IEnumerable<string> outerClassNames =
                outerClasses.Select(
                    classDeclaration => classDeclaration.Identifier.NormalizeWhitespace().ToFullString());
            return string.Join('.', outerClassNames);
        }

        private string GenerateNamespaceName(ClassDeclarationSyntax classDeclaration)
        {
            IEnumerable<NamespaceDeclarationSyntax> outerNamespaces =
                classDeclaration.AncestorsAndSelf().OfType<NamespaceDeclarationSyntax>().Reverse();
            IEnumerable<string> outerNamespaceNames =
                outerNamespaces.Select(namespaceDeclaration =>
                    namespaceDeclaration.Name.NormalizeWhitespace().ToFullString());
            return string.Join('.', outerNamespaceNames);
        }

        public string Namespace { get; }
        public string Identifier { get; }
        public string FullIdentifier { get; }

        public IEnumerable<TestingConstructorInfo> Constructors { get; }
        public TestingConstructorInfo MostParametersConstructor { get; }
    }
}