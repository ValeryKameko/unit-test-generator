using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NUnit.Framework;
using UnitTestGenerator.Entity;
using UnitTestGenerator.Generator;

namespace UnitTestGenerator.Test.Generator
{
    [TestFixture]
    public class MethodTestGeneratorTests
    {
        private const string ParametersMethod =
            @"public void Method(
                  string regularParam, 
                  in int inParam, 
                  out char outParam, 
                  ref byte refParam, 
                  params int[] paramsParam
             ) { }";

        private const string ReturnMethod = @"public string Method() { }";

        private const string ComplexMethod =
            @"public ComplexType Method(
                  string regularParam, 
                  in int inParam, 
                  out char outParam, 
                  ref byte refParam, 
                  params int[] paramsParam
             ) { }";

        private const string EmptyMethod = @"public void Method() { }";

        private MethodTestGenerator _generator;

        private static readonly CSharpParseOptions ParserOptions =
            CSharpParseOptions.Default.WithKind(SourceCodeKind.Script);

        [SetUp]
        public void SetUp() =>
            _generator = new MethodTestGenerator();

        [Test]
        public void Generate_Should_ReturnNonNullValue()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(EmptyMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            testMethod.Should().NotBeNull();
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestName()
        {
            MethodDeclarationSyntax method = GenerateMethod(EmptyMethod);
            TestingMethodInfo testingMethod = new TestingMethodInfo(method);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            testMethod.Identifier.Text.Should().Be("MethodTest");
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestingMethodCall()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(EmptyMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.DescendantNodes().OfType<InvocationExpressionSyntax>();
            invocations.Where(invocation =>
                    invocation.DescendantNodes().OfType<IdentifierNameSyntax>()
                        .Any(identifier => identifier.Identifier.Text == "Method"))
                .Should().NotBeEmpty();
        }

        [Test]
        public void Generate_Should_ReturnMethodWithNUnitTestAttribute()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(EmptyMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            testMethod.AttributeLists.SelectMany(syntax => syntax.Attributes)
                .Should().ContainSingle(attribute =>
                    (attribute.Name as IdentifierNameSyntax).Identifier.Text == "Test");
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestingMethodCallWithParameter()
        {
            TestingMethodInfo testingMethod =
                GenerateTestingMethodInfo(@"public void Method(string stringParameter) { }");

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.DescendantNodes().OfType<InvocationExpressionSyntax>();
            InvocationExpressionSyntax invocation = invocations
                .Single(invocation => invocation.DescendantNodes().OfType<IdentifierNameSyntax>()
                    .Any(identifier => identifier.Identifier.Text == "Method"));
            invocation.ArgumentList.Arguments.Select(argument => argument.NormalizeWhitespace())
                .Should().HaveCount(1);
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestingMethodCallWithParameters()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ParametersMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.DescendantNodes().OfType<InvocationExpressionSyntax>();
            IEnumerable<string> argumentNames = invocations.First().ArgumentList.Arguments.Select(
                argument => (argument.Expression as IdentifierNameSyntax)?.Identifier.Text
            );
            argumentNames.Should().BeEquivalentTo(
                "regularParam",
                "inParam",
                "outParam",
                "refParam",
                "paramsParam"
            );
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestingMethodCallWithModifiers()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ParametersMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.DescendantNodes().OfType<InvocationExpressionSyntax>();
            IEnumerable<SyntaxKind> argumentModifierKinds = invocations.First().ArgumentList.Arguments.Select(
                argument => argument.RefKindKeyword.Kind()
            );

            argumentModifierKinds.Should().BeEquivalentTo(
                SyntaxKind.None,
                SyntaxKind.InKeyword,
                SyntaxKind.OutKeyword,
                SyntaxKind.RefKeyword,
                SyntaxKind.None
            );
        }

        [Test]
        public void Generate_Should_ReturnMethodWithTestingMethodArgumentVariablesDeclarations()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ParametersMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<LocalDeclarationStatementSyntax> variableDeclarations =
                testMethod.Body.DescendantNodes()
                    .TakeWhile(node => !(node is InvocationExpressionSyntax))
                    .OfType<LocalDeclarationStatementSyntax>();
            variableDeclarations.Select(declaration => declaration.NormalizeWhitespace().ToFullString()).Should()
                .BeEquivalentTo(
                    "string regularParam = \"\";",
                    "int inParam = 0;",
                    "char outParam = '\\0';",
                    "byte refParam = 0;",
                    "int[] paramsParam = {};"
                );
        }

        [Test]
        public void Generate_Should_ReturnMethodWithExpectedArgumentVariablesDeclarations()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ParametersMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<LocalDeclarationStatementSyntax> variableDeclarations =
                testMethod.Body.DescendantNodes()
                    .SkipWhile(node => !(node is InvocationExpressionSyntax))
                    .OfType<LocalDeclarationStatementSyntax>();
            variableDeclarations.Select(declaration => declaration.NormalizeWhitespace().ToFullString()).Should()
                .BeEquivalentTo(
                    "char expectedOutParam = '\\0';",
                    "byte expectedRefParam = 0;"
                );
        }

        [Test]
        public void Generate_Should_ReturnMethodWithExpectedReturnVariableDeclarations()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ReturnMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<LocalDeclarationStatementSyntax> variableDeclarations =
                testMethod.Body.DescendantNodes()
                    .SkipWhile(node => !(node is InvocationExpressionSyntax))
                    .OfType<LocalDeclarationStatementSyntax>();
            variableDeclarations.Select(declaration => declaration.NormalizeWhitespace().ToFullString()).Should()
                .Contain("string expected = \"\";");
        }

        [Test]
        public void Generate_Should_ReturnMethodWithAsserts()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ComplexMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.Body.DescendantNodes().OfType<InvocationExpressionSyntax>();

            invocations.Select(declaration => declaration.NormalizeWhitespace().ToFullString()).Should()
                .Contain(new[]
                    {
                        "Assert.That(actual, Is.EqualTo(expected))",
                        "Assert.That(outParam, Is.EqualTo(expectedOutParam))",
                        "Assert.That(refParam, Is.EqualTo(expectedRefParam))",
                    }
                );
        }

        [Test]
        public void Generate_Should_ReturnMethodWithFailAssert()
        {
            TestingMethodInfo testingMethod = GenerateTestingMethodInfo(ComplexMethod);

            MethodDeclarationSyntax testMethod = _generator.Generate(testingMethod);

            IEnumerable<InvocationExpressionSyntax> invocations =
                testMethod.Body.DescendantNodes().OfType<InvocationExpressionSyntax>();

            invocations.Select(declaration => declaration.NormalizeWhitespace().ToFullString()).Should()
                .Contain("Assert.Fail(\"autogenerated\")");
        }

        private MethodDeclarationSyntax GenerateMethod(string methodSource)
        {
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(methodSource, ParserOptions);
            return syntaxTree.GetRoot()
                .DescendantNodes().OfType<MethodDeclarationSyntax>().First();
        }

        private TestingMethodInfo GenerateTestingMethodInfo(string methodSource)
        {
            MethodDeclarationSyntax method = GenerateMethod(methodSource);
            return new TestingMethodInfo(method);
        }
    }
}