using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NUnit.Framework;
using UnitTestGenerator.Entity;
using UnitTestGenerator.Generator;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UnitTestGenerator.Test.Generator
{
    [TestFixture]
    public class TestUnitGeneratorTests
    {
        private const string OneClassUnit =
            @"using System;
              using System.Collections.Generic;
              using System.Linq;
              using System.Text;
              namespace OneClassNamespace {
                class OneClass { }
              }";

        private const string NamespaceUnit =
            @"namespace Outer.Inner {
                class Class { }
              }";

        private const string MultipleClassesUnit =
            @"namespace Namespace {
                class FirstClass { }
                class SecondClass { }
              }";

        private const string NestedClassesUnit =
            @"namespace Namespace {
                class OuterClass {
                  class InnerClass { }
                }
              }";


        private TestUnitGenerator _generator;

        [SetUp]
        public void SetUp()
        {
            _generator = new TestUnitGenerator();
        }

        [Test]
        public void Generate_Should_ReturnSourceUnitWithUsingSection()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(OneClassUnit);

            CompilationUnitSyntax testUnit = _generator.Generate(testingUnit).Single();

            IEnumerable<UsingDirectiveSyntax> usings = testUnit.DescendantNodes().OfType<UsingDirectiveSyntax>();
            usings.Select(usingDirective => usingDirective.NormalizeWhitespace().ToFullString())
                .Should().Contain(new[]
                {
                    "using System;",
                    "using System.Collections.Generic;",
                    "using System.Linq;",
                    "using System.Text;",
                    "using Moq;",
                    "using NUnit.Framework;",
                    "using OneClassNamespace;",
                });
        }

        [Test]
        public void Generate_Should_ReturnSourceUnitWithTestNamespace()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(NamespaceUnit);

            CompilationUnitSyntax testUnit = _generator.Generate(testingUnit).Single();

            NamespaceDeclarationSyntax testNamespaceDeclaration =
                testUnit.DescendantNodes().OfType<NamespaceDeclarationSyntax>().Single();
            testNamespaceDeclaration.Name.ToFullString()
                .Should().Be("Outer.Inner.Tests");
        }

        [Test]
        public void Generate_Should_ReturnSourceUnitWithTestClass()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(NamespaceUnit);

            CompilationUnitSyntax testUnit = _generator.Generate(testingUnit).Single();

            IEnumerable<ClassDeclarationSyntax> testClassDeclarations =
                testUnit.DescendantNodes().OfType<ClassDeclarationSyntax>();
            testClassDeclarations.Should().HaveCount(1);
            testClassDeclarations.Single().Identifier.Text
                .Should().Be("ClassTests");
        }

        [Test]
        public void Generate_Should_ReturnMultipleUnitsForMultipleClasses()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(MultipleClassesUnit);

            IEnumerable<CompilationUnitSyntax> testUnits = _generator.Generate(testingUnit);

            testUnits.Should().HaveCount(2);
            IEnumerable<ClassDeclarationSyntax> classes =
                testUnits.SelectMany(testUnit => testUnit.DescendantNodes().OfType<ClassDeclarationSyntax>());
            classes.Select(classDeclaration => classDeclaration.Identifier.NormalizeWhitespace().Text)
                .Should().Contain(new[]
                {
                    "FirstClassTests",
                    "SecondClassTests",
                });
        }

        [Test]
        public void Generate_Should_ReturnMultipleUnitsForNestedClasses()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(NestedClassesUnit);

            IEnumerable<CompilationUnitSyntax> testUnits = _generator.Generate(testingUnit);

            testUnits.Should().HaveCount(2);
            IEnumerable<ClassDeclarationSyntax> classes =
                testUnits.SelectMany(testUnit => testUnit.DescendantNodes().OfType<ClassDeclarationSyntax>());
            classes.Select(classDeclaration => classDeclaration.Identifier.NormalizeWhitespace().Text)
                .Should().Contain(new[]
                {
                    "OuterClassTests",
                    "InnerClassTests",
                });
        }

        [Test]
        public void Generate_Should_ReturnMultipleUnitsWithCorrectNestedDeclarations()
        {
            TestingUnitInfo testingUnit = GenerateTestingUnit(NestedClassesUnit);

            IEnumerable<CompilationUnitSyntax> testUnits = _generator.Generate(testingUnit);

            testUnits.Should().HaveCount(2);
            IEnumerable<FieldDeclarationSyntax> fieldDeclarations =
                testUnits.SelectMany(testUnit => testUnit.DescendantNodes().OfType<FieldDeclarationSyntax>());
            fieldDeclarations.Select(fieldDeclaration =>
                    fieldDeclaration.Declaration.Type.NormalizeWhitespace().ToFullString())
                .Should().Contain(new[]
                {
                    "OuterClass",
                    "OuterClass.InnerClass",
                });
        }

        private static TestingUnitInfo GenerateTestingUnit(string unitSource) =>
            new TestingUnitInfo(ParseCompilationUnit(unitSource));
    }
}