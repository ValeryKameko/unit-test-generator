using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NUnit.Framework;
using UnitTestGenerator.Entity;
using UnitTestGenerator.Generator;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UnitTestGenerator.Test.Generator
{
    [TestFixture]
    public class ClassTestGeneratorTests
    {
        private const string EmptyClass =
            @"public class EmptyClass { }";

        private const string EmptyConstructorClass =
            @"public class EmptyConstructorClass {
                public EmptyConstructorClass() { } 
            }";

        private const string ComplexConstructorClass =
            @"public class ComplexConstructorClass {
                public ComplexConstructorClass(
                    FirstDependency firstDependency,
                    SecondDependency secondDependency,
                    int thirdParameter) { } 
            }";

        private ClassTestGenerator _generator;

        [SetUp]
        public void SetUp() =>
            _generator = new ClassTestGenerator();

        [Test]
        public void Generate_Should_ReturnTestingClass()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(EmptyClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            classTest.Identifier.Text.Should().Be("EmptyClassTests");
            classTest.AttributeLists[0].Attributes[0].Name.NormalizeWhitespace().ToFullString()
                .Should().Be("TestFixture");
        }

        [Test]
        public void Generate_Should_ReturnClassWithTestingUnderTestDeclaration()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(EmptyClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            IEnumerable<FieldDeclarationSyntax> fields =
                classTest.DescendantNodes().OfType<FieldDeclarationSyntax>();
            fields.Select(field => field.NormalizeWhitespace().ToFullString())
                .Should().Contain("private EmptyClass _emptyClassUnderTest;");
        }

        [Test]
        public void Generate_Should_ReturnClassWithClassSetupMethod()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(EmptyClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            IEnumerable<MethodDeclarationSyntax> methods =
                classTest.DescendantNodes().OfType<MethodDeclarationSyntax>();
            methods.Where(method => method.Identifier.Text == "SetUp")
                .Should().ContainSingle();

            MethodDeclarationSyntax setupMethod =
                methods.Single(method => method.Identifier.Text == "SetUp");
            setupMethod.AttributeLists.SelectMany(syntax => syntax.Attributes)
                .Should().ContainSingle(attribute =>
                    (attribute.Name as IdentifierNameSyntax).Identifier.Text == "SetUp");
        }

        [Test]
        public void Generate_Should_ReturnClassWithMockFields()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(ComplexConstructorClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            IEnumerable<FieldDeclarationSyntax> fields =
                classTest.DescendantNodes().OfType<FieldDeclarationSyntax>();
            fields.Select(field => field.NormalizeWhitespace().ToFullString())
                .Should().Contain(new[]
                {
                    "private Mock<FirstDependency> _firstDependency;",
                    "private Mock<SecondDependency> _secondDependency;",
                });
        }

        [Test]
        public void Generate_Should_ReturnClassWithTestingClassConstructorParametersDeclaration()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(ComplexConstructorClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            MethodDeclarationSyntax setUpMethod =
                classTest.DescendantNodes().OfType<MethodDeclarationSyntax>()
                    .First(method => method.Identifier.Text == "SetUp");

            setUpMethod.Body.Statements
                .Select(statement => statement.NormalizeWhitespace().ToFullString())
                .Should().Contain(new[]
                {
                    "_firstDependency = new Mock<FirstDependency>();",
                    "_secondDependency = new Mock<SecondDependency>();",
                    "int thirdParameter = 0;",
                });
        }

        [Test]
        public void Generate_Should_ReturnClassWithTestingClassConstructorInvocation()
        {
            ClassDeclarationSyntax classDeclaration = GenerateClass(ComplexConstructorClass);

            ClassDeclarationSyntax classTest =
                _generator.Generate(new TestingClassInfo(classDeclaration));

            MethodDeclarationSyntax setUpMethod =
                classTest.DescendantNodes().OfType<MethodDeclarationSyntax>()
                    .First(method => method.Identifier.Text == "SetUp");

            setUpMethod.Body.Statements
                .Select(statement => statement.NormalizeWhitespace().ToFullString())
                .Should().Contain(
                    "_complexConstructorClassUnderTest = new ComplexConstructorClass(_firstDependency.Object, _secondDependency.Object, thirdParameter);"
                );
        }
        
        private ClassDeclarationSyntax GenerateClass(string classSource) =>
            ParseMemberDeclaration(classSource) as ClassDeclarationSyntax;
    }
}