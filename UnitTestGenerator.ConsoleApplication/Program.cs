﻿using System;
using System.IO;

namespace UnitTestGenerator.ConsoleApplication
{
    internal class Program
    {
        private const string DefaultSourceDirectoryPath = ".";
        private const string DefaultSaveDirectoryPath = "./Tests";

        private static string ReadSourceDirectoryPath()
        {
            Console.Out.Write($"Enter source directory path (default '{DefaultSourceDirectoryPath}'): ");

            string path = Console.ReadLine();
            if (string.IsNullOrEmpty(path))
                path = ".";
            return path;
        }

        private static string ReadSaveDirectoryPath()
        {
            Console.Out.Write($"Enter save directory path (default '{DefaultSaveDirectoryPath}'): ");

            string path = Console.ReadLine();
            if (string.IsNullOrEmpty(path))
                path = DefaultSaveDirectoryPath;
            return path;
        }

        private static void Main(string[] args)
        {
            string sourceDirectory = ReadSourceDirectoryPath();
            string saveDirectory = ReadSaveDirectoryPath();

            TestUnitGeneratorConfig config = new TestUnitGeneratorConfig
            {
                SourceDirectory = sourceDirectory,
                TestDirectory = saveDirectory
            };
            TestUnitGenerator generator = new TestUnitGenerator(config);
            generator.Generate().Wait();
            Console.WriteLine("Tests has been generated");
        }
    }
}