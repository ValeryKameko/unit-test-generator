using System;

namespace UnitTestGenerator.ConsoleApplication
{
    public class TestUnitGeneratorConfig
    {
        public string TestDirectory { get; set; }
        public string SourceDirectory { get; set; }
        public int MaxReaderThreadCount { get; set; } = Environment.ProcessorCount;
        public int MaxGeneratorThreadCount { get; set; } = Environment.ProcessorCount;
        public int MaxWriterThreadCount { get; set; } = Environment.ProcessorCount;
    }
}