using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using UnitTestGenerator.ConsoleApplication.Pipeline;

namespace UnitTestGenerator.ConsoleApplication
{
    public class TestUnitGenerator
    {
        private ITargetBlock<string> _pipeline;
        private const string SearchPattern = "*.cs";

        private readonly string _sourceDirectory;

        public TestUnitGenerator(TestUnitGeneratorConfig config)
        {
            UnitTestPipelineConfig unitTestPipelineConfig = new UnitTestPipelineConfig
            {
                FileReaderOptions = new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = config.MaxReaderThreadCount
                },
                FileWriterOptions = new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = config.MaxWriterThreadCount
                },
                UnitTestGeneratorOptions = new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = config.MaxGeneratorThreadCount
                },
                SaveDirectory = Path.GetFullPath(config.SourceDirectory, Directory.GetCurrentDirectory())
            };
            _pipeline = UnitTestPipelineFactory.CreatePipeline(unitTestPipelineConfig);
            _sourceDirectory = config.SourceDirectory;
        }

        public async Task Generate()
        {
            string unitDirectoryPath =
                Path.GetFullPath(_sourceDirectory, Directory.GetCurrentDirectory());
            IEnumerable<string> unitPaths =
                Directory.EnumerateFiles(unitDirectoryPath, SearchPattern, SearchOption.AllDirectories);
            foreach (string unitPath in unitPaths)
                _pipeline.Post(unitPath);
            _pipeline.Complete();
            await _pipeline.Completion;
        }
    }
}