using System.Threading.Tasks.Dataflow;
using UnitTestGenerator.Entity;

namespace UnitTestGenerator.ConsoleApplication.Pipeline
{
    public static class UnitTestPipelineFactory
    {
        private static DataflowLinkOptions DefaultLinkOptions = new DataflowLinkOptions
        {
            PropagateCompletion = true
        };

        public static ITargetBlock<string> CreatePipeline(UnitTestPipelineConfig config)
        {
            IPropagatorBlock<string, CompilationItem> _reader =
                AsyncFileReaderBlockFactory.CreateAsyncFileReaderBlock(config.FileReaderOptions);
            ITargetBlock<CompilationItem> _writer =
                AsyncFileWriterBlockFactory.CreateAsyncFileWriterBlock(config.SaveDirectory, config.FileWriterOptions);
            
            IPropagatorBlock<CompilationItem, CompilationItem> unitTestTransformer = new TestUnitGeneratorBlock(config.UnitTestGeneratorOptions);
            
            _reader.LinkTo(unitTestTransformer, DefaultLinkOptions);
            unitTestTransformer.LinkTo(_writer, DefaultLinkOptions);

            return _reader;
        }
    }
}