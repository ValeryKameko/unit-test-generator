using System.IO;
using System.Threading.Tasks.Dataflow;
using UnitTestGenerator.Entity;

namespace UnitTestGenerator.ConsoleApplication.Pipeline
{
    public static class AsyncFileWriterBlockFactory
    {
        public static ITargetBlock<CompilationItem> CreateAsyncFileWriterBlock(
            string saveDirectory,
            ExecutionDataflowBlockOptions options)
        {
            return new ActionBlock<CompilationItem>(async item =>
            {
                string fullFilePath = Path.GetFullPath(item.Path, saveDirectory);
                Directory.CreateDirectory(Path.GetDirectoryName(fullFilePath));
                await File.WriteAllTextAsync(fullFilePath, item.Source);
            }, options);
        }
    }
}