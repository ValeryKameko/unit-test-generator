using System.IO;
using System.Threading.Tasks.Dataflow;
using UnitTestGenerator.Entity;

namespace UnitTestGenerator.ConsoleApplication.Pipeline
{
    public static class AsyncFileReaderBlockFactory
    {
        public static IPropagatorBlock<string, CompilationItem> CreateAsyncFileReaderBlock(ExecutionDataflowBlockOptions options)
        {
            return new TransformBlock<string, CompilationItem>(
                async filePath =>
                {
                    string content = await File.ReadAllTextAsync(filePath);
                    return new CompilationItem(filePath, content);
                },
                options);
        }
    }
}