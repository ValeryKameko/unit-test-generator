using System;
using System.IO;
using System.Threading.Tasks.Dataflow;
using static System.IO.Path;

namespace UnitTestGenerator.ConsoleApplication.Pipeline
{
    public class UnitTestPipelineConfig
    {
        public string SaveDirectory { get; set; } =
            Directory.GetCurrentDirectory();

        public ExecutionDataflowBlockOptions FileReaderOptions { get; set; } =
            DefaultExecutionDataflowBlockOptions;

        public ExecutionDataflowBlockOptions FileWriterOptions { get; set; } =
            DefaultExecutionDataflowBlockOptions;

        public ExecutionDataflowBlockOptions UnitTestGeneratorOptions { get; set; } =
            DefaultExecutionDataflowBlockOptions;

        private static ExecutionDataflowBlockOptions DefaultExecutionDataflowBlockOptions =>
            new ExecutionDataflowBlockOptions
            {
                MaxDegreeOfParallelism = Environment.ProcessorCount
            };
    }
}